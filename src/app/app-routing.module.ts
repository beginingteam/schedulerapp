import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { ProjectComponent } from './components/project/project.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { AddProjectComponent } from './components/add-project/add-project.component';
import { AddTaskComponent } from './components/add-task/add-task.component';
import { ProjectTaskComponent } from './components/project-task/project-task.component';
import { ProjectTaskDetailComponent } from './components/project-task-detail/project-task-detail.component';
import { ProjectSettingComponent } from './components/project-setting/project-setting.component';
import { ProjectTimelineComponent } from './components/project-timeline/project-timeline.component';
import { ProjectMemberComponent } from './components/project-member/project-member.component';
import { NavTabComponent } from './components/nav-tab/nav-tab.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ProjectGuard } from './guards/project.guard';
import { EditTaskComponent } from './components/edit-task/edit-task.component';

const routes: Routes = [
  {
    path: '', component: NavBarComponent, children: [
      { path: '', component: DashboardComponent },
      {
        path: 'projects/:uid', component: NavTabComponent, children:
          [
            { path: '', component: ProjectComponent },
            { path: 'member', component: ProjectMemberComponent },
            { path: 'setting', component: ProjectSettingComponent },
            { path: 'calendar', component: ProjectTimelineComponent },
            { path: 'addtask', component: AddTaskComponent },
            { path: 'tasks', component: ProjectTaskComponent },
            { path: 'task/:tuid', component: ProjectTaskDetailComponent },
            { path: 'task/:tuid/edit', component: EditTaskComponent }
          ], canActivate: [ProjectGuard]

      },
      { path: 'addproject', component: AddProjectComponent },
      { path: 'profile', component: UserProfileComponent },

    ],
    canActivate: [AuthGuard]
  },
  { path: 'auth', component: WelcomeComponent },
  { path: '404', component: NotFoundComponent },
  { path: '**', pathMatch: 'full', redirectTo: '404' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes), RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
