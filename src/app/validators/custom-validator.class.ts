import { AbstractControl } from '@angular/forms';

export const  PassMatcher = (control: AbstractControl) => {
    const password = control.get('password');
    const password_confirmation = control.get("confirmPassword");
    if(!password || !password_confirmation) return null;
    return password.value === password_confirmation.value ? null : control.get('confirmPassword').setErrors({passNotMatch: true});
};

export const DateOrder = (control: AbstractControl) =>{
    const startDate = control.get('startDate');
    const endDate = control.get('endDate');
    // console.log(`${startDate.value} : ${endDate.value}`);
    if(!startDate || !endDate) {
        return null
    };
    // return endDate.value - startDate.value > 0 ? null : control.get('endDate').setErrors({DateOrder: true});
    if(endDate.value - startDate.value > 0){
        control.get('endDate').setErrors(null)
        return null
    }
    else{
        control.get('endDate').setErrors({DateOrder: true})
    }
}