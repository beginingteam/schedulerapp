// Angular Material Components
import { MaterialModule } from './material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatNativeDateModule } from '@angular/material/core';

// Angular core Module
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Extra Angular modules
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireStorageModule } from '@angular/fire/storage';

// Other Import
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FontAwesomeModule, FaIconLibrary} from '@fortawesome/angular-fontawesome';
import { faCalendarAlt as farCalendarAlt} from '@fortawesome/free-regular-svg-icons';
import { environment } from '../environments/environment';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { DragAndDropDirective } from './directives/drag-and-drop.directive';
import { faTasks, faUsers, faCogs, faBell, faUserPlus, faCrown, faStar,
  faSearch, faTimes, faPlus, faExternalLinkAlt, faEdit} from '@fortawesome/free-solid-svg-icons';

// Guards
import { AuthGuard } from './guards/auth.guard';

// Services
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';
import { TaskService } from './services/task.service';

// Components
import { AppComponent } from './app.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { AddProjectComponent } from './components/add-project/add-project.component';
import { AddTaskComponent } from './components/add-task/add-task.component';
import { ProjectTaskComponent } from './components/project-task/project-task.component';
import { ProjectComponent } from './components/project/project.component';
import { ProjectSettingComponent } from './components/project-setting/project-setting.component';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { ProjectTimelineComponent } from './components/project-timeline/project-timeline.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { ProjectTaskDetailComponent } from './components/project-task-detail/project-task-detail.component';
import { CommentComponent } from './components/comment/comment.component';
import { ProjectMemberComponent } from './components/project-member/project-member.component';
import { NavTabComponent } from './components/nav-tab/nav-tab.component';
import { ProjectMemberListComponent } from './components/project-member-list/project-member-list.component';
import { ProjectMemberRequestComponent } from './components/project-member-request/project-member-request.component';
import { ProjectMemberAddComponent } from './components/project-member-add/project-member-add.component';
import { NotificationComponent } from './components/notification/notification.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { UploadComponent } from './components/upload/upload.component';
import { EditTaskComponent } from './components/edit-task/edit-task.component';



@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    DashboardComponent,
    SignInComponent,
    SignUpComponent,
    AddProjectComponent,
    ProjectTaskComponent,
    ProjectComponent,
    ProjectSettingComponent,
    ConfirmDialogComponent,
    ProjectTimelineComponent,
    WelcomeComponent,
    AddTaskComponent,
    ProjectTaskDetailComponent,
    ProjectMemberComponent,
    CommentComponent,
    NavTabComponent,
    ProjectMemberListComponent,
    ProjectMemberRequestComponent,
    ProjectMemberAddComponent,
    NotificationComponent,
    UserProfileComponent,
    NotFoundComponent,
    UploadComponent,
    DragAndDropDirective,
    EditTaskComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    MatNativeDateModule,
    Ng2SearchPipeModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireModule.initializeApp(environment.firebase),
    FontAwesomeModule,
    CalendarModule.forRoot({ provide: DateAdapter, useFactory: adapterFactory })
  ],
  providers: [
    AuthService,
    UserService,
    TaskService,
    AuthGuard
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ConfirmDialogComponent,
    NotificationComponent,
  ],
})
export class AppModule {
  constructor(libary: FaIconLibrary) {
    libary.addIcons(farCalendarAlt, faTasks, faUsers, faCogs,
      faBell, faUserPlus, faCrown, faStar, faSearch, faTimes, faPlus, faExternalLinkAlt, faEdit);
  }
}
