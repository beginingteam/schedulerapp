import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { User } from '../models/user.model';
import { AngularFireStorage } from '@angular/fire/storage';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private userCollection: AngularFirestoreCollection<User>;
  private users: Observable<User[]>;

  constructor(
    private firestorage: AngularFireStorage,
    private firestore: AngularFirestore,
    private authService: AuthService,
  ) {
    this.userCollection = this.firestore.collection<User>('users');
    this.users = this.userCollection.valueChanges();
  }

  getUser(uid: string): Observable<User> {
    return this.firestore.doc<User>(`users/${uid}`).valueChanges();
  }

  getCurrentUser(): Observable<User> {
    const uid = this.authService.getCurrentUserUID();
    return this.getUser(uid);
  }

  getUsers(): Observable<User[]> {
    return this.users;
  }

  updateUser(user: User): Promise<void> {
    const ref = this.firestore.doc<User>(`users/${user.uid}`);
    const data: any = {
      displayName: user.displayName,
      photoURL: user.photoURL,
    };
    return ref.update(data).then(
      () => {
        this.authService.updateAuthProfile(user);
      }
    );
  }

  createNewUser(user: User): void {
    const userRef = this.firestore.doc<User>(`users/${user.uid}`);

    const storageRef = this.firestorage.ref('profile_photo/default.png').getDownloadURL();
    storageRef.subscribe(res => {
      const data: User = {
        uid: user.uid,
        email: user.email,
        displayName: user.displayName,
        photoURL: res,
      };
      userRef.ref.get().then(doc => {
        if (!doc.exists) {
          userRef.set(data);
          this.authService.updateAuthProfile(data);
        }
      });
    });

  }
}
