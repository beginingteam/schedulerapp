import { Injectable } from '@angular/core';
import { MatDialog } from "@angular/material/dialog";
import { ConfirmDialogComponent } from "../components/confirm-dialog/confirm-dialog.component";
import { Dialog } from '../models/dialog.model';
import { NotificationComponent } from '../components/notification/notification.component';
import { UploadComponent } from '../components/upload/upload.component';
@Injectable({
  providedIn: 'root'
})
export class DialogService {
  constructor(
    private dialog : MatDialog
  ) { }

  openConfirmDialog(ref : Dialog){
    const dialogRef = this.dialog.open(
      ConfirmDialogComponent, {width: '20rem'}
    )
    dialogRef.componentInstance.setDialog(ref);
    return dialogRef;
  }

  openNotificationDialog(){
    const dialogRef = this.dialog.open(
      NotificationComponent, {width: '30rem', position: {top: '3rem'}, autoFocus: false}
    )
    return dialogRef;
  }

  openUploadDialog(){
    const dialogRef = this.dialog.open(
      UploadComponent, {width: '40rem', position:{top: '3rem'}}
    )
    return dialogRef;
  }
}
