import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { Request, requestType } from '../models/request.model';
import { Observable } from 'rxjs';
import { User } from '../models/user.model';
import { firestore } from "firebase/app";
import { ProjectService } from './project.service';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class MemberService {
  private requestURL = 'requests';
  constructor(
    private fireStore: AngularFirestore,
    private projectService: ProjectService,
    private router: Router
  ) { }

  addRequest(request: Request) : Promise<DocumentReference>{
    const ref = this.fireStore.collection<Request>(this.requestURL).add(request);
    ref.then(newRef=>{
      const reqID ={
        RequestID : newRef.id
      }
      newRef.update(reqID);
    })
    return ref;
  }
  removeRequest(reqID: String): Promise<void>{
    const ref = this.fireStore.doc<Request>(`${this.requestURL}/${reqID}`);
    return ref.delete();
  }
  //get request that sent to user.
  getProjectRequest(projectID: string): Observable<Request[]>{
    return this.fireStore.collection<Request>(
      this.requestURL, ref=>ref.where('projectID', '==', `${projectID}`)
      .where('type', '==', requestType.Request)
      ).valueChanges();
  }
  //get request that user has been requested.
  getMemberRequest(userID: string): Observable<Request[]>{
    return this.fireStore.collection<Request>(
      this.requestURL, ref=>ref.where('userID', '==', `${userID}`)
      .where('type', '==', requestType.Request)
      ).valueChanges();
  }
  getProjectInvite(projectID: string): Observable<Request[]>{
    return this.fireStore.collection<Request>(
      this.requestURL, ref=>ref.where('projectID', '==', `${projectID}`)
      .where('type', '==', requestType.Invite)
    ).valueChanges();
  }

  //Get request that invite from project manager
  getMemberInvite(userID: string): Observable<Request[]>{
    return this.fireStore.collection<Request>(
      this.requestURL, ref=>ref.where('userID', '==', `${userID}`).where('type', '==', requestType.Invite)).valueChanges();
  }

  getUserFromRequestList(requestList : Request[]): Observable<User[]>{
    const userIDList = new Array<string>();
    if(requestList.length == 0) return null;
    requestList.forEach(request => userIDList.push(request.userID));
    return this.fireStore.collection<User>('users', ref=>ref
      .where('uid', 'in', userIDList)).valueChanges();
  }
  
  addMemberTOProject(userID: string, projectID: string): Promise<void>{
    const ref = this.fireStore.doc(`projects/${projectID}`);
    return ref.update({
      member: firestore.FieldValue.arrayUnion(userID)
    });
  }
  removeMemberFromProject(userID: string, projectID: string): Promise<void>{
    const ref = this.fireStore.doc(`projects/${projectID}`);
    return ref.update({
      member: firestore.FieldValue.arrayRemove(userID)
    })
  }
  isMember(projectID: string, userID: string): Observable<boolean>{
    return this.projectService.getProject(projectID).pipe(
      map(project => {
        if (project?.member.includes(userID)) { return true; }
        else {
          alert('You do not have permision.')
          this.router.navigate(['/'])
          return false
        };
      })
    )
  }
}
