import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { Observable, of } from 'rxjs';
import { Project } from '../models/project.model';
import { User } from '../models/user.model';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  private projectURL = 'projects';

  constructor(
    private firestore: AngularFirestore,
    private router: Router
  ) { }

  addProject(project: Project): Promise<DocumentReference> {
    const ref =  this.firestore.collection<Project>(this.projectURL).add(project);
    ref.then(newRef => {
      const uid: Project = {
        uid: newRef.id,
      };
      newRef.update(uid)
    });
    return ref;
  }

  getProject(uid: string): Observable<Project> {
    return this.firestore.doc<Project>(`${this.projectURL}/${uid}`).valueChanges();
  }

  getProjctFromArray(uidList: string[]): Observable<Project[]>{
    if(uidList.length == 0) return of([])
    return this.firestore.collection<Project>(this.projectURL, ref =>
      ref.where('uid', 'in', uidList)).valueChanges();
  }

  getProjectMember(member: string[]): Observable<User[]>{
    return this.firestore.collection<User>('users', ref =>
      ref.where('uid', 'in', member)).valueChanges();
  }

  getUserProjects(uid: string): Observable<Project[]> {
    return this.firestore.collection<Project>(this.projectURL, ref =>
      ref.where('member', 'array-contains', `${uid}`)).valueChanges();
  }

  updateProject(project: any): Promise<void> {
    const ref = this.firestore.doc<Project>(`${this.projectURL}/${project.uid}`);
    return ref.update(project)
  }

  deleteProject(uid: string): Promise<void> {
    this.router.navigate(['/']);
    const ref = this.firestore.doc<Project>(`${this.projectURL}/${uid}`);
    return ref.delete();
  }

  getAllProject(): Observable<Project[]>{
    return this.firestore.collection<Project>(`${this.projectURL}`).valueChanges();
  }
}
