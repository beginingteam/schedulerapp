import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference, DocumentChangeAction } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Task } from '../models/task.model';
import { Comment } from '../models/comment.model';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private taskURL = 'tasks';
  private commentURL = 'comments';

  constructor(
    private firestore: AngularFirestore
  ) { }

  addTask(task: Task): Promise<DocumentReference> {
    const ref = this.firestore.collection<Task>(this.taskURL).add(task);
    ref.then(newRef => {
      const uid: Task = {
        uid: newRef.id,
        project: task.project
      };
      newRef.update(uid);
    });
    return ref;
  }

  getTask(uid: string): Observable<Task> {
    return this.firestore.doc<Task>(`${this.taskURL}/${uid}`).valueChanges();
  }

  getUseTask(userID: string, projectID: string): Observable<Task[]> {
    return this.firestore.collection<Task>(`${this.taskURL}`, ref =>
      ref.where('owner', '==', `${userID}`).where('project', '==', `${projectID}`)).valueChanges();
  }

  getTaskInProject(uid: string): Observable<DocumentChangeAction<Task>[]> {
    return this.firestore.collection<Task>(this.taskURL, ref =>
      ref.where('project', '==', `${uid}`)).snapshotChanges();
  }

  updateTask(task: Task): Promise<void> {
    const ref = this.firestore.doc<Task>(`${this.taskURL}/${task.uid}`);
    return ref.set(task, { merge: true });
  }

  deleteTask(uid: string): Promise<void> {
    const ref = this.firestore.doc<Task>(`${this.taskURL}/${uid}`);
    return ref.delete();
  }

  addComment(comment: Comment): void {
    this.firestore.collection<Comment>(this.commentURL).add(comment);
  }

  getComments(taskUID: string): Observable<DocumentChangeAction<Comment>[]> {
    return this.firestore.collection<Comment>(this.commentURL, ref =>
      ref.where('taskUID', '==', `${taskUID}`)).snapshotChanges();
  }
}
