import { firestore } from 'firebase/app';

export interface Task {
  uid: string;
  project: string;
  name?: string;
  description?: string;
  startDate?: firestore.Timestamp;
  endDate?: firestore.Timestamp;
  owner?: string;
  isFinished?: boolean;
}
