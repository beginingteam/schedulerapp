export interface Request {
    RequestID: string;
    userID: string;
    projectID: string;
    status: requestStatus;
    type: requestType;
}

export enum requestStatus{
    Accepted,
    Rejected,
    Pendding
}
export enum requestType{
    Invite,
    Request
}