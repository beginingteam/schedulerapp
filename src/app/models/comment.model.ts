import { firestore } from 'firebase/app';

export interface Comment {
  taskUID: string;
  userUID: string;
  content: string;
  timestamp: firestore.Timestamp;
}
