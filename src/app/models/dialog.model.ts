export interface Dialog {
    status : boolean;
    title : string;
    desc : string;
    taskID? : string;
}
