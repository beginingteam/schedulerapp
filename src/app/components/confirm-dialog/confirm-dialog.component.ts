import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { ProjectService } from 'src/app/services/project.service';
import { Dialog } from 'src/app/models/dialog.model';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.css']
})

export class ConfirmDialogComponent implements OnInit {
  dialog : Dialog;
  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogComponent>,
    public projectService: ProjectService
  ) { }

  ngOnInit() {
  }
  setDialog(ref : Dialog){
    this.dialog = ref;
  }
  closeDialog() {
    this.dialog.status = false;
    this.dialogRef.close();
  }
  confirmDialog() {
    this.dialog.status = true;
    this.dialogRef.close();
  }
}
