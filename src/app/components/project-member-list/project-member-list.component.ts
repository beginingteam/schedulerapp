import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { Project } from 'src/app/models/project.model';
import { User } from '../../models/user.model'
import { ProjectService } from 'src/app/services/project.service';
import { UserService } from 'src/app/services/user.service';
import { MemberService } from 'src/app/services/member.service';
@Component({
  selector: 'app-project-member-list',
  templateUrl: './project-member-list.component.html',
  styleUrls: ['./project-member-list.component.css']
})
export class ProjectMemberListComponent implements OnInit {
  @Input() project : Observable<Project>;
  @Output('parentFun') parentFun: EventEmitter<number> = new EventEmitter();
  members : User[];
  user: User;
  isManager : boolean = false;
  isLoading: boolean = true;
  constructor(
    private projectService: ProjectService,
    private userService: UserService,
    private memberService: MemberService
  ) { 

  }
  
  ngOnInit(): void {
    this.project.subscribe(project=>{
      this.projectService.getProjectMember(project?.member)
      .subscribe(members=>{
        this.members=members;
        this.userService.getCurrentUser().subscribe(user=>{
          this.isManager = user.uid === project.manager;
          this.user = user;
         this.isLoading = false;

       })
      })
    });
  }
  changeTab(index: number){
    this.parentFun.emit(index);
  }

  removeMember(user: User){
    this.project.subscribe(project=>this.memberService.
      removeMemberFromProject(user.uid, project.uid));
  }
}
