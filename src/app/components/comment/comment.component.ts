import { Component, OnInit, Input } from '@angular/core';
import { Comment } from 'src/app/models/comment.model';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {
  @Input() comment: Comment;
  user: User;

  constructor(
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.userService.getUser(this.comment.userUID).subscribe(user => {
      this.user = user;
    });
  }

}
