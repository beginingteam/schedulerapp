import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import { ProjectService } from 'src/app/services/project.service';
import { Project } from '../../models/project.model';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { DialogService } from 'src/app/services/dialog.service';
import { MemberService } from 'src/app/services/member.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  isExpanded = false;
  displayNanme: string;
  projectList: Observable<Project[]>;
  user: User;
  selectedProjectID: string;
  isLoading: boolean = true;
  notiNumber: number;
  notiVisible: boolean = false;
  constructor(
    private authService: AuthService,
    private userService: UserService,
    private projectService: ProjectService,
    private router: Router,
    private dialogService: DialogService,
    private memberService: MemberService
  ) { }

  ngOnInit() {
    this.userService.getCurrentUser().subscribe(user => {
      if (user) {
        this.user = user;
        this.memberService.getMemberInvite(user.uid).subscribe(res => {
          this.notiNumber = res.length
          this.notiVisible = this.notiNumber == 0 ? true : false;
          this.projectList = this.projectService.getUserProjects(user.uid);
          this.projectList.subscribe(() => this.isLoading = false);
        });
      }
    });

  }

  logOut() {
    this.authService.logOut();
    this.router.navigate(['/auth']);
  }

  toggleMenu() {
    this.isExpanded = !this.isExpanded;
  }

  openNotification() {
    this.dialogService.openNotificationDialog();
  }
}
