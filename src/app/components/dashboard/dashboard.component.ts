import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Project } from 'src/app/models/project.model';
import { ProjectService } from '../../services/project.service';
import { User } from 'src/app/models/user.model';
import { MemberService } from 'src/app/services/member.service';
import { Request, requestType, requestStatus } from 'src/app/models/request.model';
import { SnackbarService } from 'src/app/services/snackbar.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  searchText: string;
  allProject: Project[];

  searchBox: any;

  user: User;
  allUser: User[];
  userRequest: Request[];

  todayDate = new Date();

  isSearching: boolean = false;
  isLoading: boolean = true;
  isLoadingProjectList: boolean = true;

  userProject: Project[];
  constructor(
    private userService: UserService,
    private projectService: ProjectService,
    private memberService: MemberService,
    public  snackBarService: SnackbarService
  ) { }

  ngOnInit() {
    this.userService.getCurrentUser().subscribe(
      user => {
        this.user = user
        this.projectService.getUserProjects(user.uid).subscribe(projects => {
          this.userProject = projects;
          this.isLoading = false;
        });
      });
  }

  onFocus() {
    // console.log('on focus');
    this.isSearching = true;
    this.projectService.getAllProject().subscribe(allProject => {
      this.userService.getUsers().subscribe(allUser => {
        this.memberService.getMemberRequest(this.user.uid).subscribe(allRequest => {
          this.userRequest = allRequest
          this.allProject = allProject;
          this.allUser = allUser
          this.isLoadingProjectList = false;
        });
      });
    })
  }
  onBlur() {
    if (this.searchText)
      this.searchText = '';
    this.isSearching = false;
  }

  getUserName(userID: string) {
    if (this.allUser) return this.allUser.find(user => user.uid === userID)?.displayName;
    else return '';
  }

  sendRequest(projectID: string) {
    var memberRequest: Request = {
      RequestID: '',
      projectID: projectID,
      userID: this.user.uid,
      type: requestType.Request,
      status: requestStatus.Pendding,
    }
    this.memberService.addRequest(memberRequest);
  }

  isMember(project: Project): boolean {
    if (project.member.includes(this.user.uid)) return true;
    else return false;
  }

  hasSendRequest(projectID: string): boolean {
    if (this.userRequest)
      if (this.userRequest.find(request => request.projectID === projectID)) return true;
    return false;
  }

  cancelRequest(projectID: string) {
    const reqID = this.userRequest.find(request => request.projectID === projectID).RequestID;
    this.memberService.removeRequest(reqID);
  }

  getProjectProgress(project: Project, progress?: boolean) {
    var rawTimeRemaining = (project?.endDate.toDate().getTime() - Date.now()) / (1000 * 3600 * 24);
    var allTime = (project.endDate.toDate().getTime() - project.startDate.toDate().getTime()) / (1000 * 3600 * 24);

    var late = false;
    if (rawTimeRemaining < 0) late = true;
    else late = false;

    if (progress) {
      return {
        time: late ? 100 : Math.ceil((allTime - rawTimeRemaining) * 100 / allTime),
        isLate: late
      }
    }
    return {
      time: Math.floor(Math.abs(rawTimeRemaining)),
      isLate: late
    };
  }

  getColorStatus(project: Project): string{
    if(this.isMember(project)){
      return 'green'
    }
    else if(this.hasSendRequest(project.uid)){
      return 'orange'
    }
    else{
      return 'blue'
    }
  }

}
