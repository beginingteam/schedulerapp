import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from 'src/app/models/user.model';
import { Router, ActivatedRoute } from '@angular/router';
import { TaskService } from 'src/app/services/task.service';
import { UserService } from 'src/app/services/user.service';
import { DateOrder } from 'src/app/validators/custom-validator.class';
import { ProjectService } from 'src/app/services/project.service';
import { Task } from 'src/app/models/task.model';

@Component({
  selector: 'app-edit-task',
  templateUrl: './edit-task.component.html',
  styleUrls: ['./edit-task.component.css']
})
export class EditTaskComponent implements OnInit {
  projectUID: string;
  taskUID: string;
  memberList: User[] = [];
  editTaskForm: FormGroup;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private activeRoute:  ActivatedRoute,
    private taskService: TaskService,
    private userService: UserService,
    private projectService: ProjectService
  ) {
    this.editTaskForm = this.formBuilder.group(
      {
        name: ['', [Validators.required]],
        description: [''],
        startDate: ['', [Validators.required]],
        endDate: ['', [Validators.required]],
        owner: ['', [Validators.required]]
      }, {validators: [DateOrder]}

    );
  }

  ngOnInit(): void {
    this.activeRoute.parent.params.subscribe(routeParams => {
      this.projectUID = routeParams.uid;
      this.projectService.getProject(routeParams.uid).subscribe(project => {
        project.member.map(member => this.userService.getUser(member).subscribe(
          user => this.memberList.push(user)));
        this.activeRoute.params.subscribe(localRouteParams => {
          this.taskUID = localRouteParams.tuid;
          this.taskService.getTask(localRouteParams.tuid).subscribe(task => {
            this.editTaskForm.setValue({
              name: task.name,
              description: task.description,
              owner: task.owner,
              startDate: task.startDate?.toDate(),
              endDate: task.endDate?.toDate()
            });
          });
        });
      });
    });

  }


  onSubmit(): void {
    const updatedTask: Task = {
      uid: this.taskUID,
      name: this.editTaskForm.value.name,
      project: this.projectUID,
      description: this.editTaskForm.value.description,
      startDate: this.editTaskForm.value.startDate,
      owner: this.editTaskForm.value.owner,
      endDate: this.editTaskForm.value.endDate,
    };
    this.taskService.updateTask(updatedTask).then(() =>
      this.onReturn());
  }

  onReturn(): void {
    this.router.navigate([`/projects/${this.projectUID}/task/${this.taskUID}`]);
  }

}
