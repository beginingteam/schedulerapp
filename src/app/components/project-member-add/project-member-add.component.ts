import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable, from, of } from 'rxjs';
import { Project } from 'src/app/models/project.model';
import { FormControl } from '@angular/forms';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';
import { Request, requestStatus, requestType } from 'src/app/models/request.model';
import { MemberService } from 'src/app/services/member.service';

@Component({
  selector: 'app-project-member-add',
  templateUrl: './project-member-add.component.html',
  styleUrls: ['./project-member-add.component.css']
})
export class ProjectMemberAddComponent implements OnInit {
  @Input() project : Observable<Project>;
  @Output('parentFun') parentFun: EventEmitter<number> = new EventEmitter();
  user: Observable<User>;
  users: Observable<User[]>;
  joinRequest: Observable<Request[]>;
  inviteRequest: Observable<Request[]>;
  optionList: Observable<User[]>;
  selectedUser = new FormControl();
  isSelected : boolean = false;
  rawProject: Project;

  RequestForm : Request = {
    RequestID : '',
    userID : '',
    projectID: '',
    status: requestStatus.Pendding,
    type: requestType.Invite
  }

  constructor(
    private userService: UserService,
    private memberService: MemberService,
  ) { }

  ngOnInit(): void {
    this.user = this.userService.getCurrentUser();
    this.users = this.userService.getUsers();
    this.project.subscribe(project=>{
      this.RequestForm.projectID = project.uid;
      // this.optionList.subscribe(res=>console.log(res))
      this.inviteRequest = this.memberService.getProjectInvite(project.uid);
      this.joinRequest = this.memberService.getProjectRequest(project.uid);
      this.rawProject = project;
      this.optionList = this.getUserNotInProject(project);

    })
  }

  changeTab(index: number){
    this.parentFun.emit(index);
  }

  sendRequest(){
    const reqForm = this.RequestForm;
    reqForm.userID = this.selectedUser.value.uid;
    if(this.isSelected){this.memberService.addRequest(reqForm);}
    this.selectedUser = new FormControl();
    this.isSelected = false;
    this.changeTab(1);
  }
 
  reloadOption(){
    this.optionList = this.getUserNotInProject(this.rawProject);
  }

  setUser(){
    this.isSelected = true;    
  }
  unsetUser(){
    this.isSelected = false;
    this.selectedUser = new FormControl();
  }

  getUserNotInProject(project: Project): Observable<User[]>{
    var inProject = new Array<string>();
    const notInProject = new Array<User>();
    this.inviteRequest.subscribe(req => req.forEach(ele => inProject.push(ele.userID)));
    this.joinRequest.subscribe(req => req.forEach(ele => inProject.push(ele.userID)));
    // console.log(notInProject);
    // console.log(inProject);
    this.users.subscribe(users => {
      users.forEach(user => {
        if (!inProject.includes(user.uid) && !project.member.includes(user.uid))
          notInProject.push(user)
      })
    })
    return of(notInProject);
  }
}
