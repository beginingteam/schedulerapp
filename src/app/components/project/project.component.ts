import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Project } from 'src/app/models/project.model';
import { ProjectService } from 'src/app/services/project.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {
  project?: Observable<Project>
  timeRemaining: number
  late: boolean;
  tabIndex: number = 0;

  isLoading: boolean = true;

  constructor(
    private activeRoute: ActivatedRoute,
    private projectService: ProjectService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.activeRoute.params.subscribe(routeParams =>{
        //console.log(`${routeParams.uid} : ${routeParams.project_n}`)
        this.project = this.projectService.getProject(routeParams.uid);
        this.project?.subscribe(() => this.isLoading = false)
        this.project?.subscribe(project => {
          if(project){
            var rawTimeRemaining = (project?.endDate.toDate().getTime() - Date.now()) / (1000 * 3600 * 24);
            if(rawTimeRemaining < 0) this.late = true;
            else this.late = false;
            this.timeRemaining = Math.floor(Math.abs(rawTimeRemaining));
          }

        });
      });
    this.activeRoute.paramMap.pipe(map(() => window.history.state))
      .subscribe(data => {
      if (data.tabIndex !== undefined) {
      }
        this.tabIndex = data.tabIndex;
    })
  }

  isProjectManager(): boolean {
    return false;
  }
}
