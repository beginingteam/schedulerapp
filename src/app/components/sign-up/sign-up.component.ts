import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { PassMatcher } from "../../validators/custom-validator.class";

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public authService: AuthService
  ) {
    this.registerForm = this.formBuilder.group({
      displayName: ['', [Validators.required]],
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.minLength(8), Validators.required]],
      confirmPassword: ['', [Validators.required]]
    }, { validator: [PassMatcher]});
  }

  registerForm: FormGroup;
  passwordCheck: Validators;

  ngOnInit() {
  }


  onSubmit(formDirective: FormGroupDirective) {
    this.tryRegister(this.registerForm.value);
    this.registerForm.reset();
    formDirective.resetForm();
  }

  tryRegister(input: { displayName: string; email: string; password: string; }) {
    this.authService.register(input.displayName, input.email, input.password).then(
      () => {
        this.router.navigate(['/']);
      }
    ).catch(
      err => {
        console.error(err);
        alert(err);
      }
    );
  }
}
