import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';
import { ProjectService } from 'src/app/services/project.service';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as _moment from 'moment';
import { default as _rollupMoment } from 'moment';
import { Location } from "@angular/common";
import { DateOrder } from '../../validators/custom-validator.class'
import { Observable } from 'rxjs';
import { Project } from 'src/app/models/project.model';
import { ActivatedRoute, Router } from '@angular/router';
import { DialogService } from 'src/app/services/dialog.service';
import { Dialog } from 'src/app/models/dialog.model';
import { MemberService } from 'src/app/services/member.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-project-setting',
  templateUrl: './project-setting.component.html',
  styleUrls: ['./project-setting.component.css'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class ProjectSettingComponent implements OnInit {
  user: User;
  projectForm : FormGroup;
  project?: Observable<Project>;
  projectRaw? : Project;
  editRef : Dialog = {
    title: "Change project detail",
    desc: "Confirm to change this project detail",
    status: false
  };
  deleteRef : Dialog = {
    title: "Delete Project",
    desc: "Are you sure to DELETE this project?",
    status: false
  }
  leaveRef : Dialog ={
    title: "Leave Project",
    desc: "Are you sure to LEAVE this project?",
    status: false
  }
  isLoading : boolean = true;
  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private projectService: ProjectService,
    private location: Location,
    private activeRoute: ActivatedRoute,
    private dialogService: DialogService,
    private router: Router,
    private memberService: MemberService,
    private snackService: SnackbarService
  ) {
    this.projectForm = this.formBuilder.group(
      {
        name: [{value:''}, [Validators.required]],
        startDate: [{value:''}, [Validators.required]],
        endDate: [{value:''}, [Validators.required] ],
      }, {validators: [DateOrder]}
    );
  }


  ngOnInit() {
    this.userService.getCurrentUser().subscribe(user => {
      this.user = user;
    });
    this.activeRoute.parent.params.subscribe(routeParams =>{
      this.project = this.projectService.getProject(routeParams.uid);
    });
    this.project.subscribe(project =>{
      this.projectRaw = project;
      this.projectForm.setValue({
        name: typeof project === 'undefined' ? '' : project?.name,
        startDate: moment(project?.startDate?.toDate()),
        endDate: moment(project?.endDate?.toDate())
      });
      this.isLoading = false;
    });

  }

  leaveProject(){
    console.log("leave Project")
    const diaRef = this.dialogService.openConfirmDialog(this.leaveRef);
    diaRef.afterClosed().subscribe(()=>{
      if(diaRef.componentInstance.dialog.status){
        this.memberService.removeMemberFromProject(this.user.uid, this.projectRaw.uid);
        this.router.navigate(['/']);
      }
    })
  }
  onSubmit() {
    const diaRef = this.dialogService.openConfirmDialog(this.editRef);
    diaRef.afterClosed().subscribe(() =>{
      if(diaRef.componentInstance.dialog.status)
        this.updateProjectDetail();
    })
  }
  updateProjectDetail(){
    const temp = {
      uid: this.projectRaw.uid,
      name: this.projectForm.value.name,
      startDate: this.projectForm.value.startDate.toDate(),
      endDate: this.projectForm.value.endDate.toDate(),
    }
    console.log(temp);
    const uid = this.projectService.updateProject(temp);
    uid.then(()=>this.snackService.openSnackBar('Project has been updated.', 'success'))
  }
  deleteProject(){
    const diaRef = this.dialogService.openConfirmDialog(this.deleteRef);
    diaRef.afterClosed().subscribe(()=>{
      if(diaRef.componentInstance.dialog.status){
        this.projectService.deleteProject(this.projectRaw.uid).then(
          ()=>this.snackService.openSnackBar('Project has been deleted.', 'alert')
        );

      }
    })
  }



  backLastPage():void{
    this.location.back();
  }

  isManager(): boolean{
    if(this.projectRaw.manager === this.user.uid)return true;
    this.projectForm.disable();
    return false;
  }
}
