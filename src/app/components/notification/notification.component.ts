import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MemberService } from 'src/app/services/member.service';
import { Observable } from 'rxjs';
import { Request } from 'src/app/models/request.model';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user.model';
import { ProjectService } from 'src/app/services/project.service';
import { Project } from 'src/app/models/project.model';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {
  user: User
  allUser: User[];
  projectList: Project[];
  isLoading: boolean = true;
  requestList: Request[];

  constructor(
    private dialogRef: MatDialogRef<NotificationComponent>,
    private memberService: MemberService,
    private userService: UserService,
    private projectService: ProjectService
  ) { }

  ngOnInit(): void {
    this.userService.getCurrentUser().subscribe(user => {
      this.user =user;
      this.memberService.getMemberInvite(user.uid).subscribe(requestList => {
        this.requestList = requestList;
        this.projectService.getProjctFromArray(requestList.map(request => request.projectID)).subscribe(
          projectList => {
            this.projectList = projectList;
            this.userService.getUsers().subscribe(users=> {
              this.allUser = users
              this.isLoading = false;
            });
          }
        )
      });
    });
  }

  closeDialog() {
    this.dialogRef.close();
  }

  getUserName(userID: string){
    return this.allUser.find(user=> user.uid === userID)?.displayName;
  }

  acceptRequest(projectID: string){
    this.memberService.addMemberTOProject(this.user.uid, projectID);
    this.memberService.removeRequest(this.requestList.find(req=> req.projectID === projectID)?.RequestID);
  }

  rejectRequest(projectID: string){
    this.memberService.removeRequest(this.requestList.find(req=> req.projectID === projectID)?.RequestID);
  }
}
