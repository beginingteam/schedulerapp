import { Component, ViewChild, TemplateRef, OnInit } from '@angular/core';
import { isSameDay, isSameMonth, } from 'date-fns';
import { Subject } from 'rxjs';
import { CalendarEvent, CalendarView } from 'angular-calendar';
import { TaskService } from 'src/app/services/task.service';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user.model';
import { Task } from 'src/app/models/task.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Project } from 'src/app/models/project.model';
import { ProjectService } from 'src/app/services/project.service';
const colors: any[] = [
  {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
];

@Component({
  selector: 'app-project-timeline',
  templateUrl: './project-timeline.component.html',
  styleUrls: ['./project-timeline.component.css']
})
export class ProjectTimelineComponent implements OnInit {
  @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any>;
  view: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;
  isLoading: boolean = true;
  viewDate: Date = new Date();
  modalData: {
    action: string;
    event: CalendarEvent;
  };
  project: Project
  user: User;
  userTask: Task[];
  events: CalendarEvent[];

  constructor(
    private taskService: TaskService,
    private userService: UserService,
    private activeRoute: ActivatedRoute,
    private projectService: ProjectService,
    private router: Router
  ) { }

  ngOnInit() {
    this.userService.getCurrentUser().subscribe(user => {
      this.user = user;
      this.activeRoute.parent.params.subscribe(params => {
        this.projectService.getProject(params.uid).subscribe(
          project => this.project = project
        );
        this.taskService.getUseTask(user.uid, params.uid).subscribe(tasks => {
          this.userTask = tasks;
          var i = -1;
          this.events = tasks.map(task => {
            i++
            return {
              id: task.uid,
              title: task.name,
              start: task.startDate.toDate(),
              end: task.endDate.toDate(),
              color: colors[i]
            }
          });
          this.isLoading = false;

        })
      })

    })
  }

  refresh: Subject<any> = new Subject();



  activeDayIsOpen: boolean = true;


  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }


  onClick(event: CalendarEvent){
    console.log('on Click');
    this.router.navigate([`projects/${this.project.uid}/task/${event.id}`])

  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }
}
