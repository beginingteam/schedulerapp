import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { storage } from "firebase/app";
import { SnackbarService } from 'src/app/services/snackbar.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<UploadComponent>,
    private storage: AngularFireStorage,
    private snackService: SnackbarService
  ) { }

  ngOnInit(): void {
  }
  files: File[] = [];
  selectFile: File;
  percentage: Observable<number>;
  task: AngularFireUploadTask;
  downloadURL: Observable<string>
  isUploading: boolean = false;

  uploadFile(event: FileList) {
    for (let index = 0; index < event.length; index++) {
      const element = event[index];
      this.files.push(element)
      this.selectFile = element;
    }  
  }
  deleteAttachment(index) {
    this.files.splice(index, 1)
  }
  selectAttachment(index){
    this.selectFile = this.files[index]
  }

  uploadFileToStorage(){
    this.isUploading = true;
    const path = `profile_photo/${Date.now()}_${this.selectFile.name}`;
    const ref = this.storage.ref(path);
    this.task = this.storage.upload(path, this.selectFile);
    this.percentage = this.task.percentageChanges();
    this.task.then(res=>{
      this.downloadURL = ref.getDownloadURL();
      this.snackService.openSnackBar('Uploading has finished', 'success');
      this.closeDialog();
    })
  }

  closeDialog(){
    this.dialogRef.close()
  }
}
