import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';
import { Project } from 'src/app/models/project.model';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-nav-tab',
  templateUrl: './nav-tab.component.html',
  styleUrls: ['./nav-tab.component.css']
})
export class NavTabComponent implements OnInit {
  project: Project;
  isHome: boolean = true;
  constructor(
    private activeRoute: ActivatedRoute,
    private projectService: ProjectService,
    private location: Location,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.activeRoute.params.subscribe(routeParams => {
      this.projectService.getProject(routeParams.uid).subscribe(project =>this.project = project)

    });
    this.router.events.subscribe(event=>{
      this.isHome = this.activeRoute.children[0].routeConfig.component.name === 'ProjectComponent'? true: false; 
    })

  }

  back(): void{
    this.location.back();
  }
}
