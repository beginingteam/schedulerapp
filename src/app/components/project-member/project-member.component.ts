import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Project } from 'src/app/models/project.model';
import { ProjectService } from 'src/app/services/project.service';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-project-member',
  templateUrl: './project-member.component.html',
  styleUrls: ['./project-member.component.css']
})
export class ProjectMemberComponent implements OnInit {
  project : Observable<Project>
  mananger : string;
  selected : number = 0;
  isLoading : boolean = true;
  user: User;
  constructor(
    private userService: UserService,
    private projectService: ProjectService,
    private activeRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.activeRoute.parent.params.subscribe(routeParams =>{
      this.project = this.projectService.getProject(routeParams.uid);
      this.userService.getCurrentUser().subscribe(user =>{
        this.user = user;
        this.project.subscribe(res=>{
          this.isLoading = false;
          this.mananger = res.manager;
        });
      })
    });
  }
  changeTab(index: number){
    // console.log(`hello I'm your parent. ${index}`) 
    this.selected = index; 
  }

  isManager() : boolean{
    if(this.mananger === this.user?.uid) return true;
    return false;
  }
}
