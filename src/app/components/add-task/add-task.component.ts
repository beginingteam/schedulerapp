import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TaskService } from 'src/app/services/task.service';
import { UserService } from 'src/app/services/user.service';
import { ProjectService } from 'src/app/services/project.service';
import { User } from 'src/app/models/user.model';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { DateOrder } from 'src/app/validators/custom-validator.class';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {
  projectUID: string;
  memberList: User[] = [];
  addTaskForm: FormGroup;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private activeRoute: ActivatedRoute,
    private taskService: TaskService,
    private projectService: ProjectService,
    private userService: UserService,
    private snackService: SnackbarService
  ) {

    this.addTaskForm = this.formBuilder.group(
      {
        name: ['', [Validators.required]],
        description: [''],
        startDate: ['', [Validators.required]],
        endDate: ['', [Validators.required]],
        owner: ['', [Validators.required]]
      }, {validators: [DateOrder]}
    );
  }

  ngOnInit(): void {
    this.activeRoute.parent.params.subscribe(routeParams => {
      this.projectUID = routeParams.uid;
      this.projectService.getProject(routeParams.uid).subscribe(project => {
        project.member.map(member => this.userService.getUser(member).subscribe(
          user => this.memberList.push(user)));
      });
    });
  }

  onSubmit(): void {
    this.taskService.addTask(
      {
        uid: '',
        project: this.projectUID,
        name: this.addTaskForm.value.name,
        description: this.addTaskForm.value.description,
        owner: this.addTaskForm.value.owner,
        isFinished: false,
        startDate: this.addTaskForm.value.startDate,
        endDate: this.addTaskForm.value.endDate
      }
    ).then(() => this.snackService.openSnackBar('Task has beed created.', 'success'));
    this.onReturn();
  }

  onReturn(): void {
    this.router.navigate([`/projects/${this.projectUID}/tasks`]);
  }
}
