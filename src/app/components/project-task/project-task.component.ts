import { Component, OnInit } from '@angular/core';
import { TaskService } from 'src/app/services/task.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Task } from 'src/app/models/task.model';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user.model';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-project-task',
  templateUrl: './project-task.component.html',
  styleUrls: ['./project-task.component.css']
})
export class ProjectTaskComponent implements OnInit {
  projectUID: string;
  isLoading = true;
  isManager: boolean;
  userList: User[];
  taskList: Task[];

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private taskService: TaskService,
    private userService: UserService,
    private projectServuce: ProjectService
  ) { }

  ngOnInit(): void {
    this.activeRoute.parent.params.subscribe(routeParams => {
      this.projectUID = routeParams.uid;
      this.projectServuce.getProject(this.projectUID).subscribe(project =>
        this.userService.getCurrentUser().subscribe(user =>
          this.isManager = user.uid === project.manager));
      this.taskService.getTaskInProject(this.projectUID).subscribe(taskListDoc => {
        this.taskList = taskListDoc.map(task => ({
          ...task.payload.doc.data()
        }));
        this.userService.getUsers().subscribe(users =>
          this.userList = users);
        this.isLoading = false;
      });
    });
  }

  getUsername(uid: string): string {
    if (uid) {
      return this.userList?.find(user => user.uid === uid)?.displayName;
    } else {
      return 'None';
    }
  }

  onClick(taskUID: string): void {
    this.router.navigate([`/projects/${this.projectUID}/task/${taskUID}`]);
  }

  onAddTask(): void {
    this.router.navigate([`/projects/${this.projectUID}/addtask`]);
  }
}
