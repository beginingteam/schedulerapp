import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { Project } from 'src/app/models/project.model';
import { Request, requestType } from 'src/app/models/request.model';
import { MemberService } from 'src/app/services/member.service';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-project-member-request',
  templateUrl: './project-member-request.component.html',
  styleUrls: ['./project-member-request.component.css']
})
export class ProjectMemberRequestComponent implements OnInit {

  @Input() project: Observable<Project>

  requestList: Observable<Request[]>
  inviteList: Observable<Request[]>
  userRequest: Observable<User[]>
  userInvite: Observable<User[]>

  constructor(
    private memberService: MemberService,
  ) { }

  ngOnInit(): void {
    this.project.subscribe(project=>{
      this.requestList = this.memberService.getProjectRequest(project.uid);
      this.inviteList = this.memberService.getProjectInvite(project.uid);
      this.requestList.subscribe(requests=>{
        this.userRequest = this.memberService.getUserFromRequestList(requests);
      });
      this.inviteList.subscribe(invites=>{
        this.userInvite = this.memberService.getUserFromRequestList(invites);
      })
    })
  }

  removeInvite(request: Request): void{
    this.memberService.removeRequest(request.RequestID);
  }

  acceptRequest(request: Request): void{
    this.memberService.addMemberTOProject(request.userID, request.projectID)
    .then(() =>this.memberService.removeRequest(request.RequestID));
  }

  rejectRequest(request: Request): void{
    this.memberService.removeRequest(request.RequestID).then(() =>this.memberService.removeRequest(request.RequestID));;    
  }

}
