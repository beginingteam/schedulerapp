import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Task } from 'src/app/models/task.model';
import { TaskService } from 'src/app/services/task.service';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Comment } from 'src/app/models/comment.model';
import { firestore } from 'firebase/app';
import { Dialog } from 'src/app/models/dialog.model';
import { DialogService } from 'src/app/services/dialog.service';
import { ProjectService } from 'src/app/services/project.service';
import { SnackbarService } from 'src/app/services/snackbar.service';

@Component({
  selector: 'app-project-task-detail',
  templateUrl: './project-task-detail.component.html',
  styleUrls: ['./project-task-detail.component.css']
})
export class ProjectTaskDetailComponent implements OnInit {
  task: Task;
  owner: User;
  comments: Comment[];
  currentUserUID: string;
  commentForm: FormGroup;
  isProjectManager: boolean;
  canDoneTask: boolean;
  deleteRef: Dialog = {
    title: 'Delete Task',
    desc: 'Do you really want to delete this task?',
    status: false
  };

  constructor(
    private activeRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private taskService: TaskService,
    private userService: UserService,
    private projectService: ProjectService,
    private dialogService: DialogService,
    private snackbarService: SnackbarService,
    private router: Router
  ) {
    this.commentForm = this.formBuilder.group(
      {
        comment: ['', Validators.required]
      }
    );
  }

  ngOnInit(): void {
    this.activeRoute.params.subscribe(routeParams => {
      this.taskService.getTask(routeParams.tuid).subscribe(task => {
        this.task = task;
        if (task?.owner) {
          this.userService.getUser(task.owner).subscribe(user => {
            this.owner = user;
          });
        }
        this.taskService.getComments(this.task.uid).subscribe(commentsDoc => {
          this.comments = commentsDoc.map(comment => comment.payload.doc.data());
          this.comments.sort((a, b) => a.timestamp.seconds - b.timestamp.seconds);
        });
      });
    });
    this.activeRoute.parent.params.subscribe(projectParams =>
      this.projectService.getProject(projectParams.uid).subscribe(project =>
        this.userService.getCurrentUser().subscribe(user => {
          this.currentUserUID = user.uid;
          this.isProjectManager = project.manager === user.uid;
          this.canDoneTask = this.isProjectManager || this.task?.owner === user.uid;
        })));
  }

  getStartDateString(): string {
    if (this.task?.startDate) {
      return this.task.startDate.toDate().toDateString();
    } else {
      return '-';
    }
  }

  getEndDateString(): string {
    if (this.task?.endDate) {
      return this.task.endDate.toDate().toDateString();
    } else {
      return '-';
    }
  }

  getOwnerName(): string {
    if (this.owner) {
      return this.owner.displayName;
    } else {
      return 'None';
    }
  }

  getTimeRemaingString(): string {
    if (this.task?.isFinished) {
      return 'Task Finished';
    }
    const timeDiff = (this.task?.endDate.toDate().getTime() - Date.now()) / (1000 * 3600 * 24);
    const isLate = timeDiff < 0;
    const formattedTimeDiff = Math.floor(Math.abs(timeDiff));
    if (!isLate) {
      return `Time remaining: ${formattedTimeDiff} days`;
    } else {
      return `Late: ${formattedTimeDiff} days`;
    }
  }

  onPostComment(): void {
    const newCommnet: Comment = {
      taskUID: this.task.uid,
      userUID: this.currentUserUID,
      content: this.commentForm.value.comment,
      timestamp: firestore.Timestamp.now()
    };
    this.taskService.addComment(newCommnet);
    this.commentForm.reset();
  }

  onEditTask(): void {
    this.router.navigate([`/projects/${this.task.project}/task/${this.task.uid}/edit`]);
  }

  onDeleteTask(): void {
    const diaRef = this.dialogService.openConfirmDialog(this.deleteRef);
    diaRef.afterClosed().subscribe(() => {
      if (diaRef.componentInstance.dialog.status) {
        this.taskService.deleteTask(this.task.uid);
        this.onReturn();
      }
    });
  }

  onTaskDone(): void {
    const updatedTask: Task = {
      uid: this.task.uid,
      project: this.task.project,
      isFinished: !this.task.isFinished,
    };

    this.taskService.updateTask(updatedTask).then(() => {
      if (this.task.isFinished) {
        this.snackbarService.openSnackBar('Task Done!', 'success');
      } else {
        this.snackbarService.openSnackBar('Task Reopen!', 'success');
      }
    });
  }

  onReturn(): void {
    this.router.navigate([`/projects/${this.task.project}/tasks`]);
  }
}
