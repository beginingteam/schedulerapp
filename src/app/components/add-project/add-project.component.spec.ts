import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddProjectComponent } from './add-project.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { UserService } from 'src/app/services/user.service';
import { ProjectService } from 'src/app/services/project.service';
import { Router } from '@angular/router';
import { SnackbarService } from 'src/app/services/snackbar.service';

describe('AddProjectComponent', () => {

    let component: AddProjectComponent;
    let fixture: ComponentFixture<AddProjectComponent>;


    let serviceStub: any;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AddProjectComponent],
            providers: [
                { provide: UserService, useValue: serviceStub },
                { provide: ProjectService, useValue: serviceStub },
                { provide: Router, useValue: serviceStub },
                { provide: SnackbarService, useValue: serviceStub }
            ],
            imports: [
                BrowserModule,
                FormsModule,
                ReactiveFormsModule,
            ]
        })
            .compileComponents().then(() => {
                fixture = TestBed.createComponent(AddProjectComponent);
                component = fixture.componentInstance;


            });
    }));
    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('form should be invalid (endDate > startDate)', async(() => {
        let startDate = new Date()
        let endDate = new Date(); endDate.setDate(startDate.getDate()-2)
        component.addProjectForm.controls['name'].setValue('Project #1')
        component.addProjectForm.controls['startDate'].setValue(startDate)
        component.addProjectForm.controls['endDate'].setValue(endDate)
        expect(component.addProjectForm.valid).toBeFalsy();
    }))

    it('form should be invalid (endDate = startDate)', async(() => {
        let startDate = new Date()
        component.addProjectForm.controls['name'].setValue('Project #Star')
        component.addProjectForm.controls['startDate'].setValue(startDate)
        component.addProjectForm.controls['endDate'].setValue(startDate)
        expect(component.addProjectForm.valid).toBeFalsy();
    }))

    it('form should be invalid (name is empty)', async(() => {
        let startDate = new Date()
        let endDate = new Date(); endDate.setDate(startDate.getDate()+1)
        component.addProjectForm.controls['name'].setValue('')
        component.addProjectForm.controls['startDate'].setValue(startDate)
        component.addProjectForm.controls['endDate'].setValue(endDate)
        expect(component.addProjectForm.valid).toBeFalsy();
    }))

    it('form should be invalid (endDate is empty)', async(() => {
        let startDate = new Date()
        component.addProjectForm.controls['name'].setValue('Test')
        component.addProjectForm.controls['startDate'].setValue(startDate)
        component.addProjectForm.controls['endDate'].setValue('')
        expect(component.addProjectForm.valid).toBeFalsy();
    }))

    it('form should be invalid (startDate is empty)', async(() => {
        let startDate = new Date()
        let endDate = new Date(); endDate.setDate(startDate.getDate()+1)
        component.addProjectForm.controls['name'].setValue('Test')
        component.addProjectForm.controls['startDate'].setValue('')
        component.addProjectForm.controls['endDate'].setValue(endDate)
        expect(component.addProjectForm.valid).toBeFalsy();
    }))

    it('form should be invalid (Empty Form)', async(() => {
        component.addProjectForm.controls['name'].setValue('')
        component.addProjectForm.controls['startDate'].setValue('')
        component.addProjectForm.controls['endDate'].setValue('')
        expect(component.addProjectForm.valid).toBeFalsy();
    }))

    it('form should be valid', async(() => {
        let startDate = new Date()
        let endDate = new Date(); endDate.setDate(startDate.getDate()+1)
        component.addProjectForm.controls['name'].setValue('Project')
        component.addProjectForm.controls['startDate'].setValue(startDate)
        component.addProjectForm.controls['endDate'].setValue(endDate)
        expect(component.addProjectForm.valid).toBeTruthy();
    }))


});