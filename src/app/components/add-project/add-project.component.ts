import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';
import { ProjectService } from 'src/app/services/project.service';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as _moment from 'moment';
import { default as _rollupMoment } from 'moment';
import { Location } from "@angular/common";
import { DateOrder } from '../../validators/custom-validator.class'
import { Router } from '@angular/router';
import { SnackbarService } from 'src/app/services/snackbar.service';

const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.css'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})

export class AddProjectComponent implements OnInit {
  searchName: string;
  addProjectForm: FormGroup;
  user: User;
  isSubmit: boolean = false;
  
  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private projectService: ProjectService,
    private location: Location,
    private router: Router,
    private snackService: SnackbarService
  ) {
    this.addProjectForm = this.formBuilder.group(
      {
        name: [{value:'', disabled: false}, [Validators.required]],
        startDate: [{value:'', disabled: false}, [Validators.required]],
        endDate: [{value:'', disabled: false}, [Validators.required] ],
      }, {validators: [DateOrder]}
    );
  }

  ngOnInit() {
    this.userService.getCurrentUser().subscribe(
      user => {
        this.user = user
      });

  }
  onSubmit() {
    this.isSubmit = true;
    const uid = this.projectService.addProject({
      uid: '',
      name: this.addProjectForm.value.name,
      startDate: this.addProjectForm.value.startDate === '' ? new Date() : this.addProjectForm.value.startDate.toDate(),
      endDate: this.addProjectForm.value.endDate === '' ? new Date() : this.addProjectForm.value.endDate.toDate(),
      manager: this.user.uid,
      member: [this.user.uid],
      description: ''
    });
    uid.then(res=>{
      this.snackService.openSnackBar('Project has been created.', 'success')
      this.router.navigate([`projects/${res.id}`])
    })

  }
  backLastPage() {
    this.location.back();
  }
}
