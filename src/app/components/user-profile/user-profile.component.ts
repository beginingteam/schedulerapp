import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user.model';
import { DialogService } from 'src/app/services/dialog.service';
import { SnackbarService } from 'src/app/services/snackbar.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  user: User;
  isLoading: boolean = true;
  photoURL: string;
  displayName: string;

  editing: boolean = false;
  constructor(
    private userService: UserService,
    private dialogService: DialogService,
    private snackService: SnackbarService
  ) { }

  ngOnInit(): void {
    this.userService.getCurrentUser().subscribe(user=>{
      this.user = user;
      this.isLoading = false;
      this.displayName = user.displayName;
      this.photoURL = user.photoURL; 
    })
  }

  openUploadDialog(){
    var diaRef = this.dialogService.openUploadDialog()
    diaRef.afterClosed().subscribe(()=>{
      diaRef.componentInstance.downloadURL.subscribe(res=>{
        this.photoURL = res
      })
    })
  }

  saveProfile(): void{
    const user: User = this.user;
    this.user.photoURL = this.photoURL;
    this.user.displayName = this.displayName;
    this.userService.updateUser(user).then(
      ()=>this.snackService.openSnackBar("Your profile has been saved", "success")
    );
  }
  
}
